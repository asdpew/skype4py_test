import skype
from flask import Flask, request, jsonify
from flask import request

app = Flask(__name__)

@app.route('/')
def hello():
	return "nothing to see here!"

@app.route('/send', methods=['POST', 'GET'])
def send():
	if request.method == 'POST':
		skype.sendMessage(request.form['user'], request.form['message'])
		return "" #jsonify(success=True)
	else:
		return "No!"

# main
if __name__ == '__main__':
	app.run()